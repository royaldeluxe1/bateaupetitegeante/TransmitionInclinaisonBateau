/************************************************************************************
 * 	
 * 	Name    : MMA8453_n0m1 Library Example: DataMode                       
 * 	Author  : Noah Shibley, NoMi Design Ltd. http://n0m1.com                       
 *		    : Michael Grant, Krazatchu Design Systems. http://krazatchu.ca/
 * 	Date    : May 5th 2013                                    
 * 	Version : 0.2                                              
 * 	Notes   : Arduino Library for use with the Freescale MMA8453Q via Arduino native WIRE with repeated start (was i2c of DSS circuits). 
 *
 ***********************************************************************************/
//accélérometre
#include <Wire.h>
#include <MMA8453_n0m1.h>

MMA8453_n0m1 accel;

//Liason série software
#include <SoftwareSerial.h>
#define rxPin 2
#define txPin 3

SoftwareSerial mySerial =  SoftwareSerial(rxPin, txPin);

const int numReadings = 100; //npmbre de valeur pour la moyenne

int readingsX[numReadings];      // the readings from the analog input
int readingsY[numReadings];      // the readings from the analog input
int readingsZ[numReadings];      // the readings from the analog input
int index = 0;                  // the index of the current reading
int totalX = 0;                  // the running total
int totalY = 0;                  // the running total
int totalZ = 0;                  // the running total
int averageX = 0;                // the average
int averageY = 0;                // the average
int averageZ = 0;                // the average


void setup()
{
  Serial.begin(9600);
     // define pin modes for tx, rx:
  pinMode(rxPin, INPUT);
  pinMode(txPin, OUTPUT);
  // set the data rate for the SoftwareSerial port
  mySerial.begin(9600);
  accel.setI2CAddr(0x1D); //change your device address if necessary, default is 0x1C
  accel.dataMode(true, 2); //enable highRes 10bit, 2g range [2g,4g,8g]
//  Serial.println("MMA8453_n0m1 library");
//  Serial.println("XYZ Data Example");
//  Serial.println("n0m1.com & krazatchu.ca");

  // initialize all the readings to 0: 
  for (int thisReading = 0; thisReading < numReadings; thisReading++){
    readingsX[thisReading] = 0;
    readingsY[thisReading] = 0;
    readingsX[thisReading] = 0;
  }

}

void loop()
{
  accel.update();
  
 // subtract the last reading:
  totalX= totalX - readingsX[index];
  totalY= totalY - readingsY[index];
  totalZ= totalZ - readingsZ[index];         
  // read from the sensor:  
  readingsX[index] = accel.x();
  readingsY[index] = accel.y();
  readingsZ[index] = accel.z(); 
  // add the reading to the total:
  totalX= totalX + readingsX[index];
  totalY= totalY + readingsY[index];
  totalZ= totalZ + readingsZ[index]; 
  // advance to the next position in the array:  
  index = index + 1;                    

  // if we're at the end of the array...
  if (index >= numReadings){
    // ...wrap around to the beginning:
    index = 0;
    //Mappage et limitation des valeurs mayennes de X Y Z vers ue valeur de type byte
    byte accX = map(averageX,-260,260,5,250);
    byte accY = map(averageY,-260,260,5,250);
    byte accZ = map(averageZ,-260,260,5,250);
    accX = constrain (accX, 5, 250);
    accY = constrain (accY, 5, 250);
    accZ = constrain (accZ, 5, 250);
//    Serial.print("  averageX  ");//Serial.print(averageX);
//    Serial.print(accX);
//    Serial.print("  averageY  ");//Serial.print(averageY);
//    Serial.print(accY);
//    Serial.print("  averageZ  ");//Serial.print(averageZ);
//    Serial.println(accZ);

    mySerial.write (255);
    mySerial.write (accX);
    mySerial.write (accY);
//    mySerial.write (accZ);
    mySerial.write (1);
    

  } 

  // calculate the average:
  averageX = totalX / numReadings;
  averageY = totalY / numReadings;
  averageZ = totalZ / numReadings;

  

delay (1);
}



